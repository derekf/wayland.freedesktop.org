��e$      �docutils.nodes��document���)��}�(�	rawsource�� ��children�]�(h �block_quote���)��}�(hhh]�(h �substitution_definition���)��}�(h�,.. |git_version| replace:: :commit:`67b2e32`�h]�h �	reference���)��}�(h�git commit 67b2e32�h]�h �Text����git commit 67b2e32�����}�(hh�parent�huba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]��internal���refuri��?https://gitlab.freedesktop.org/libinput/libinput/commit/67b2e32�u�tagname�hh hubah!}�(h#]�h%]�h']��git_version�ah)]�h+]�uh0h�source��<rst_prolog>��line�Kh hubh)��}�(h�^.. |git_version_full| replace:: :commit:`<function get_git_version_full at 0x7fd1528816a8>`


�h]�h)��}�(h�<git commit <function get_git_version_full at 0x7fd1528816a8>�h]�h�<git commit <function get_git_version_full at 0x7fd1528816a8>�����}�(hhh h?ubah!}�(h#]�h%]�h']�h)]�h+]��internal���refuri��ihttps://gitlab.freedesktop.org/libinput/libinput/commit/<function get_git_version_full at 0x7fd1528816a8>�uh0hh h;ubah!}�(h#]�h%]�h']��git_version_full�ah)]�h+]�uh0hh8h9h:Kh hubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h	h hhhh8Nh:Nubh �target���)��}�(h�.. _timestamps:�h]�h!}�(h#]�h%]�h']�h)]�h+]��refid��
timestamps�uh0h]h:Kh hhhh8�6/home/whot/code/libinput/build/doc/user/timestamps.rst�ubh �section���)��}�(hhh]�(h �title���)��}�(h�
Timestamps�h]�h�
Timestamps�����}�(hhuh hshhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh hnhhh8hkh:Kubh^)��}�(h�.. _event_timestamps:�h]�h!}�(h#]�h%]�h']�h)]�h+]�hi�event-timestamps�uh0h]h:Kh hnhhh8hkubhm)��}�(hhh]�(hr)��}�(h�Event timestamps�h]�h�Event timestamps�����}�(hh�h h�hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh h�hhh8hkh:Kubh �	paragraph���)��}�(hX&  Most libinput events provide a timestamp in millisecond and/or microsecond
resolution. These timestamp usually increase monotonically, but libinput
does not guarantee that this always the case. In other words, it is possible
to receive an event with a timestamp earlier than the previous event.�h]�hX&  Most libinput events provide a timestamp in millisecond and/or microsecond
resolution. These timestamp usually increase monotonically, but libinput
does not guarantee that this always the case. In other words, it is possible
to receive an event with a timestamp earlier than the previous event.�����}�(hh�h h�hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh h�hhubh�)��}�(h��For example, if a touchpad has :ref:`tapping` enabled, a button event may have a
lower timestamp than an event from a different device. Tapping requires the
use of timeouts to detect multi-finger taps and/or :ref:`tapndrag`.�h]�(h�For example, if a touchpad has �����}�(h�For example, if a touchpad has �h h�hhh8Nh:Nub�sphinx.addnodes��pending_xref���)��}�(h�:ref:`tapping`�h]�h �inline���)��}�(hh�h]�h�tapping�����}�(hhh h�ubah!}�(h#]�h%]�(�xref��std��std-ref�eh']�h)]�h+]�uh0h�h h�ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�hʌrefexplicit���	reftarget��tapping��refdoc��
timestamps��refwarn��uh0h�h8hkh:Kh h�ubh�� enabled, a button event may have a
lower timestamp than an event from a different device. Tapping requires the
use of timeouts to detect multi-finger taps and/or �����}�(h�� enabled, a button event may have a
lower timestamp than an event from a different device. Tapping requires the
use of timeouts to detect multi-finger taps and/or �h h�hhh8Nh:Nubh�)��}�(h�:ref:`tapndrag`�h]�h�)��}�(hh�h]�h�tapndrag�����}�(hhh h�ubah!}�(h#]�h%]�(hɌstd��std-ref�eh']�h)]�h+]�uh0h�h h�ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�h�refexplicit��hٌtapndrag�h�h�h݈uh0h�h8hkh:Kh h�ubh�.�����}�(h�.�h h�hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh h�hhubh�)��}�(h�CConsider the following event sequences from a touchpad and a mouse:�h]�h�CConsider the following event sequences from a touchpad and a mouse:�����}�(hj  h j  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh h�hhubh �literal_block���)��}�(h��Time      Touchpad      Mouse
---------------------------------
t1       finger down
t2        finger up
t3                     movement
t4       tap timeout�h]�h��Time      Touchpad      Mouse
---------------------------------
t1       finger down
t2        finger up
t3                     movement
t4       tap timeout�����}�(hhh j  ubah!}�(h#]�h%]�h']�h)]�h+]��	xml:space��preserve�uh0j  h:K!h h�hhh8hkubh�)��}�(hXz  For this event sequence, the first event to be sent to a caller is in
response to the mouse movement: an event of type
**LIBINPUT_EVENT_POINTER_MOTION** with the timestamp t3.
Once the timeout expires at t4, libinput generates an event of
**LIBINPUT_EVENT_POINTER_BUTTON** (press) with a timestamp t1 and an event
**LIBINPUT_EVENT_POINTER_BUTTON** (release) with a timestamp t2.�h]�(h�wFor this event sequence, the first event to be sent to a caller is in
response to the mouse movement: an event of type
�����}�(h�wFor this event sequence, the first event to be sent to a caller is in
response to the mouse movement: an event of type
�h j,  hhh8Nh:Nubh �strong���)��}�(h�!**LIBINPUT_EVENT_POINTER_MOTION**�h]�h�LIBINPUT_EVENT_POINTER_MOTION�����}�(hhh j7  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j5  h j,  ubh�W with the timestamp t3.
Once the timeout expires at t4, libinput generates an event of
�����}�(h�W with the timestamp t3.
Once the timeout expires at t4, libinput generates an event of
�h j,  hhh8Nh:Nubj6  )��}�(h�!**LIBINPUT_EVENT_POINTER_BUTTON**�h]�h�LIBINPUT_EVENT_POINTER_BUTTON�����}�(hhh jJ  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j5  h j,  ubh�* (press) with a timestamp t1 and an event
�����}�(h�* (press) with a timestamp t1 and an event
�h j,  hhh8Nh:Nubj6  )��}�(h�!**LIBINPUT_EVENT_POINTER_BUTTON**�h]�h�LIBINPUT_EVENT_POINTER_BUTTON�����}�(hhh j]  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j5  h j,  ubh� (release) with a timestamp t2.�����}�(h� (release) with a timestamp t2.�h j,  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K$h h�hhubh�)��}�(h�[Thus, the caller gets events with timestamps in the order t3, t1, t2,
despite t3 > t2 > t1.�h]�h�[Thus, the caller gets events with timestamps in the order t3, t1, t2,
despite t3 > t2 > t1.�����}�(hjx  h jv  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K+h h�hhubeh!}�(h#]�(h��id2�eh%]�h']�(�event timestamps��event_timestamps�eh)]�h+]�uh0hlh hnhhh8hkh:K�expect_referenced_by_name�}�j�  h�s�expect_referenced_by_id�}�h�h�subeh!}�(h#]�(hj�id1�eh%]�h']��
timestamps�ah)]��
timestamps�ah+]�uh0hlh hhhh8hkh:K�
referenced�Kj�  }�j�  h_sj�  }�hjh_subeh!}�(h#]�h%]�h']�h)]�h+]��source�hkuh0h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hqN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j�  �error_encoding��UTF-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�hk�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�N�gettext_compact��ub�reporter�N�indirect_targets�]��substitution_defs�}�(h5hhTh;u�substitution_names�}�(�git_version�h5�git_version_full�hTu�refnames�}��refids�}�(hj]�h_ah�]�h�au�nameids�}�(j�  hjj�  h�j�  j�  u�	nametypes�}�(j�  �j�  �j�  Nuh#}�(hjhnj�  hnh�h�j�  h�u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �id_start�K�parse_messages�]�h �system_message���)��}�(hhh]�h�)��}�(h�-Duplicate implicit target name: "timestamps".�h]�h�1Duplicate implicit target name: “timestamps”.�����}�(hhh j!  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j  ubah!}�(h#]�h%]�h']�h)]�h+]�j�  a�level�K�type��INFO��source�hk�line�Kuh0j  h hnhhh8hkh:Kuba�transform_messages�]�(j  )��}�(hhh]�h�)��}�(hhh]�h�0Hyperlink target "timestamps" is not referenced.�����}�(hhh j?  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j<  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type�j7  �source�hk�line�Kuh0j  ubj  )��}�(hhh]�h�)��}�(hhh]�h�6Hyperlink target "event-timestamps" is not referenced.�����}�(hhh jY  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h jV  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type�j7  �source�hk�line�Kuh0j  ube�transformer�N�
decoration�Nhhub.